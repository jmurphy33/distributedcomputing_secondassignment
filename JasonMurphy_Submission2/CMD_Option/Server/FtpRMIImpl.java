import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;

public class FtpRMIImpl extends UnicastRemoteObject implements FtpRMI {

    LinkedList<String> connected_users;
    String root = "";
    String name, password, fileName, bytesToSave;

    // constructor for the implementation class
    public FtpRMIImpl(String name, String rootPath) throws RemoteException {
        super(); // call the constructor of the parent class
        try {
            // setting the root path for the folder of user folders
            root = rootPath;

            // a list of users logged in to the server
            connected_users = new LinkedList<String>();

            // binding the name to the RMI registry to allow remotely calling methods and finding server
            Naming.rebind(name, this);
            System.out.println();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // allows a client to log in to the server
    @Override
    public Boolean login(String username, String pw) throws RemoteException {
        try {
            // sets the username
            String name = username;

            // sets the password
            String password = pw;

            // creates folder/checks if folder exists
            Boolean isCreated = checkDirectory(root + "/" + name);

            // if successfully found/created add user to connected users list
            if (isCreated) connected_users.add(name);

            System.out.println(name + " connected to server");
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    // allows a client to log out of the server
    @Override
    public Boolean logout(String username) throws RemoteException {
        // a check is done to see if the user is logged in
        if (connected_users.contains(username)) {

            // remove the user from the connected user list
            connected_users.remove(username);
            System.out.println(username + " disconnected from server");
            // return successful to client
            return true;
        } else  return false; // return unsuccessful to client
    }

    // allows a file to be uploaded to the server
    @Override
    public Boolean upLoad(String user, String fileName, String file) throws RemoteException {
        try {
            // a check is done to see if the user is logged in
            if (connected_users.contains(user)) {
                System.out.println(user + " wants to upload");

                // convert file to bytes
                byte[] byteArray = file.getBytes();

                // save file to the users folder
                saveFile(byteArray, user, fileName);

                // return successful to client
                return true;
            } else {
                // return unsuccessful to client
                return false;
            }
        } catch (Exception e) {
            // return unsuccessful to client
            return false;
        }
    }

    // allows a file to be downloaded from the server
    @Override
    public String download(String user, String fileName) throws RemoteException {
        try {
            // a check is done to see if the user is logged in.
            if (connected_users.contains(user)) {
                System.out.println(user + " wants to download");

                // a file object is created using the root, the name of the user and the file name
                File f = new File(root + "/" + user + "/" + fileName);

                // if the file doesn't exists
                if (!f.exists()) {
                    System.out.println(f.getAbsoluteFile() + " does not exist");
                    // return unsuccessful to the client, in the form of a blank string
                    return null;
                } else {


                    // if found, convert the file to bytes
                    byte[] b = Files.readAllBytes(Paths.get(root + "/" + user + "/" + fileName));

                    // decode the file to the correct format
                    String decoder2 = new String(b, "UTF-8");

                    // remove null characters
                    decoder2 = removeNulls(decoder2);

                    // return successful to the client, in the form of file bytes in a string
                    return decoder2;
                }
            } else {
                // return unsuccessful to the client, in the form of a blank string
                return null;
            }
        } catch (Exception e) {
            // return unsuccessful to the client, in the form of a blank string
            return null;
        }
    }

    // allow for the client to view their files in their folder
    @Override
    public String viewFiles(String user) throws RemoteException {
        try {
            // check the user is logged in
            if (connected_users.contains(user)) {
                // retrieve a list of their files
                String fileList = getFileList(user);

                // return the files found, if any...
                return fileList;
            } else {
                // return an empty string for unsuccessful
                return "";
            }
        } catch (Exception e) {
            // return an empty string for unsuccessful
            return "";
        }
    }

    private String getFileList(String name) {
        String allFiles = "";
        try {
            // get the path of the root combined with the user name
            String path = root + "\\" + name;
            // get the folder using the path
            File folder = new File(path);
            // get an array of files
            File[] listOfFiles = folder.listFiles();

            // build string with all files in directory
            for (int i = 0; i < listOfFiles.length; i++) {
                allFiles = allFiles + "\n" + listOfFiles[i].getName();
            }
            // if no files build other string
            if (listOfFiles.length == 0) allFiles = "";

        } catch (Exception e) {
            System.out.println(e.getMessage());

        }
        return allFiles;
    }

    // checks to see if a folder exists for user. If does do nothing, id doesn't create folder.
    private Boolean checkDirectory(String name) {
        try {
            File f = new File(name);
            // if already exists return successful
            if (f.exists()) {
                return true;
            } else {
                boolean isCreated = new File(name).mkdirs(); // if doesn't exist, create it.

                // if something was created return success
                if (isCreated) {
                    System.out.println("\ncreated folder for user: " + name);
                    return true;
                } else {
                    System.out.println("\nproblem with: " + name);
                    // return unsuccesful
                    return false;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        // return unsuccessful
        return false;
    }

    // removes unwanted null characters from file bytes
    private String removeNulls(String msg) {
        String ans = msg.replace("\u0000", "");
        return ans;
    }

    // saves file to user folder using the file bytes and file name
    private void saveFile(byte[] fileBytes, String name, String fileName) {
        try {
            // output stream to write to user folder with file name
            FileOutputStream fos = new FileOutputStream(root + "\\" + name + "\\" + fileName);
            // write using file bytes
            fos.write(fileBytes);
            // close stream
            fos.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
