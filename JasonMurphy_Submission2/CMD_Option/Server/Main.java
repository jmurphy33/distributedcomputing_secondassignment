import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.RMISecurityManager;

public class Main {
    static String rootPath = "", nameForServer = "";

    public static void main(String[] args) {
	// write your code here

        try{
            // allows the user to enter input
            InputStreamReader is = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(is);

            // creating the registry on port 1099
            LocateRegistry.createRegistry(1099);
            // setting the security policy the the file within the folder
            System.setProperty("java.security.policy","file:java.policy");
            // sets the security manager
            System.setSecurityManager(new RMISecurityManager());

            System.out.println("Path to store files (leave blank to store @ C:/JasonsServer): ");

            // the root path will equal the entered value or a default value
            rootPath = checkVariable(br.readLine(), "C:/JasonsServer");

            // the folder is created
            createRoot(rootPath);

            // the name to register to the RMI Registry
            System.out.println("Enter name for server's RMI Registry: (leave blank for myRMI)");

            // if left blank a default value is used
            nameForServer = checkVariable(br.readLine(), "myRMI");

            // the implementation class is instantiated and starts listening
            FtpRMIImpl imp = new FtpRMIImpl(nameForServer,rootPath);

            System.out.println("Server ready.");
        }
        catch (Exception e){
            System.out.println(e.getMessage());

        }

    }

    // this will create root directory for all user folders to go under
    static private void createRoot(String root) {
        boolean isCreated = new File(root).mkdirs();

        // if something was created print it out.
        if (isCreated) {
            System.out.println("root director created at:" + root);
        }
    }

    // this will set a default value if the entered value is blank
    static private String checkVariable(String input, String output) {
        // if the user inputs an empty string this will correct to a default value

        if (input.equals("")) {
            return output;
        } else return input;
    }
}
