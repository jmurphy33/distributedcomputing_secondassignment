package com.company;


import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.*;

public class Main {

    static InputStreamReader is = new InputStreamReader(System.in);
    static BufferedReader br = new BufferedReader(is);
    static String host, user, password, msg, serverName = "";
    static FtpRMI s;

    static void setPolicies() {
        System.setProperty("java.security.policy", "file:java.policy");
        System.setSecurityManager(new RMISecurityManager());
    }

    // sets the details to login with and calls lookup
    static void setLoginDetails() throws Exception {
        System.out.println("* Host Address [localhost]: ");
        host = checkVariable(br.readLine(), "localhost");

        System.out.println("* Server name [myRMI]: ");
        serverName = checkVariable(br.readLine(), "myRMI");

        s = (FtpRMI) Naming.lookup("rmi://" + host + "/" + serverName);

        System.out.println("* Username: ");
        user = checkUsername(br.readLine(), br);

        System.out.println("* Password: ");
        password = checkVariable(br.readLine(), "a");
    }

    // handles view files functionality
    static Boolean handleViewFiles() throws Exception {
        // boolean to check for files present on server folder
        Boolean hasFiles = true;
        // get the names of the files for user on server
        String result = s.viewFiles(user);
        // if the string is blank, no files exist for them
        if (result.equals("")) {
            // set result for message
            result = " No files found...";
            // set boolean to reflect no files found
            hasFiles = false;
        }
        System.out.println("Files available on server:" + result);

        // return boolean for files found
        return hasFiles;
    }

    // handles download functionality
    static void handleDownload() throws Exception {

        // if there are files for this user
        if (handleViewFiles()) {
            System.out.println("Include the extension. Example test.txt ");
            System.out.println("Exclude any file paths. Example do not use Hello\\test.txt ");
            System.out.println("File name to download:");

            // enter one of the files found
            String filename = br.readLine();

            // get the file from the server
            String fileMessage = (s.download(user, filename));

            // if string with file isn't null
            if(fileMessage != null){
                // select somewhere to save file
                String path = getFolder();
                // save file using filesString, path selected and the name of the file

                saveFile(fileMessage, path, filename);
            }
            else{
                System.out.println("File not found...");
            }
        }
    }

    // handles upload functionality
    static void handleUpload() throws Exception {
        // get a file from pc
        File f = getFile();
        // if successfully uploaded print success
        // upload using filename and filebytes
        if (s.upLoad(user, f.getName(), convertFile(f))) {
            System.out.println("Upload successful");
        }
    }

    // handles the logout functionality
    static void handleLogout() throws Exception {
       // if the server returns true that the user has disconnected
        if (s.logout(user)) {
            // end program
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        try {

            // sets up security manager
            setPolicies();

            // calls lookup and sets details to login to server with
            setLoginDetails();

            // if the login failed
            if (!s.login(user, password)) {
                // exit program
                System.out.println("Login failed");
                System.exit(0);
            } else {
                // if successful

                System.out.println("Now logged in...");
                System.out.println("-----------------------------------------------\n" +
                        "* Would you like to:\nUpload (1)\nView Files & Download (2)\nView files (3)\nLogout(4)");

                // select upload/download/view files/logout
                String option = br.readLine();

                // while logout isn't selected
                while (!option.equals("4")) {

                    // if upload is selected
                    if (option.equals("1")) {
                        handleUpload();
                    }

                    // if download is selected
                    if (option.equals("2")) {
                        handleDownload();
                    }

                    // if view files is selected
                    if (option.equals("3")) {
                        handleViewFiles();
                    }

                    System.out.println("-----------------------------------------------\n" +
                            "* Would you like to:\nUpload (1)\nView Files & Download (2)\nView files (3)\nLogout(4)");

                    // select another option
                    option = br.readLine();
                }

                // if logout is selected
                if (option.equals("4")) {
                    handleLogout();
                }
            }

        } catch (Exception e) {
            System.out.print("error:" + e.getMessage());
            System.exit(0);

        }
    }

    // handles saving file locally
    static void saveFile(String bytesToSave, String downloadPath, String fileName) throws Exception {

        // convert sting of file bytes to byte array
        byte[] byteArray = bytesToSave.getBytes();
        // set the download path for the file
        String path = downloadPath + "\\" + fileName;
        System.out.println("Saved to: " + path);
        // stream to write file
        FileOutputStream fos = new FileOutputStream(path);
        fos.write(byteArray);
        // close stream
        fos.close();
    }

    // converts file to string of bytes...used in upload
    static String convertFile(File file) throws Exception {
        byte[] b = Files.readAllBytes(Paths.get(file.getPath().toString()));
        String decoder = new String(b, "UTF-8");
        decoder = removeNulls(decoder);
        return decoder;
    }

    // checks if variable is left as empty string
    static String checkVariable(String input, String output) {
        if (input.equals("")) {
            return output;
        } else return input;
    }

    // user enters blank name, re-enter name
    static String checkUsername(String user, BufferedReader br) throws Exception {
        while (user.equals("")) {
            System.out.println("* Username cannot be left blank re-enter username: ");
            user = br.readLine();
        }
        return user;
    }

    // gets file for upload
    static File getFile() {
        System.out.println("JFileChoose should now appear. If it doesn't press alt+tab to switch to it...");
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text");
        chooser.setFileFilter(filter);
        int app = chooser.showOpenDialog(null);

        if (app == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            return f;
        }
        return null;
    }

    // gets folder for download
    static String getFolder() {
        System.out.println("JFileChoose should now appear. If it doesn't press alt+tab to switch to it...");
        JFileChooser chooser = new JFileChooser();
        //chooser.setCurrentDirectory(new java.io.File("C/"));
        chooser.setDialogTitle("chooser");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);

        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            System.out.println(chooser.getSelectedFile().getAbsolutePath());
            return chooser.getSelectedFile().getAbsolutePath();
        } else return "";

    }

    // removes unwanted null characters
    static String removeNulls(String msg) {
        String ans = msg.replace("\u0000", "");
        return ans;
    }
}
