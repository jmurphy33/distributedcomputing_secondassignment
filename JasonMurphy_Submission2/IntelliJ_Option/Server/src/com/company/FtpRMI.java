package com.company;

/**
 * Created by jm on 17/11/2015.
 */
public interface FtpRMI extends  java.rmi.Remote{

    Boolean login(String username, String pw)  throws java.rmi.RemoteException;

    Boolean logout(String username)  throws java.rmi.RemoteException;

    Boolean upLoad(String user, String fileName, String file) throws java.rmi.RemoteException;

    String download(String user, String fileName)throws java.rmi.RemoteException;

    String viewFiles(String user) throws java.rmi.RemoteException;
}


